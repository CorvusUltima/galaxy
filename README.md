 ## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Note](#note)
* [Info](#info)

## General info
The project aims to develop 2d software based && 
inspired by Chilli tutorials for creating standalone 3d software.

This game is a testing ground for various classes and functions that will later be added to the D&C framework.
Some of the classes that cannot be used in further development will remain only in the Galaxy project.

D&C framework https://gitlab.com/CorvusUltima/framework/-/blob/master/README.md
D&C 3D Softver development project code name E.P.I.C is under project planing right now .


For more information you can see the introduction to the series:
https://www.youtube.com/watch?v=PwuIEMUFUnQ

The goal of this software is exclusively to learn and master  concepts within software development and this is a working version of the same in constant updating .

NOTE:Chili Framework in the basic version  comes with the following features:

Gfx: putpixel / screenwidth / height
Color: getting / setting RGB color components
Mouse / kbd: input (polled and event-driven)
Sound: loading and playing sounds

	    
## Technologies
Upgraded version of the basic Chilli Framework created by the D&C team.

C++
VS 2019


## D&C team

D&C is a duo of software developers.

Luka Bucan:

Dedicated and very committed team member with great attention to detail and monitoring of project development.

With relatively little experience in programming,he already largely produces amazing software solutions that can have a realistic application to various projects.

Nemanja Pavlovic:

Motivated team member, always ready for additional work and learning new skills that can be applied both to the project itself and to personal development in software waters.

Unconventional, practical and strategic thinking contributes to overcoming challenges in everyday work.

 
Luka Bucan: bucan.luka88@gmail.com

Nemanja Pavlovic: pavlovicn88@gmail.com


	
## Setup
To run this project, install it locally using GitLab link URL ;


## Note
The project has certain shortcomings that were originally created by the limits of the Framework itself, as well as the knowledge that the D&C team possessed at the time of project planning.

Glaxy2 is currently under construction and has the following shortcomings corrected:

It will provide better quality documentation for inspection and review.

img class that stores image through the puthPixel command will go down in history,
 by applying an upgrade to software/framwork that will allow images to be read directly from the file.
Upgrade will also make it easier to create animations.

Classes will be organized according to the principle of one class, one functionality.
together with abstraction through virtual classes, this will give our code much more flexibility.

The current Bug that prevents the code from being run through the EXE file will be removed (Win 32 application caused Bug by incorrectly done  modification )

The naming standard will be clearly defined to allow better traceability of the code.



## Info
Updated version of  the Framework have the following classes  as addition.

NOTE :Many classes from the original Framework
  also have certain added features/functions in testing (Graphics!!)

 *   [Boss.h](Engine/Boss.h).
 *   [Boss.cpp](Engine/Boss.cpp).
    
 *   [Bullet.h](Engine/Bullet.h).
 *   [Bullet.cpp](Engine/Bullet.cpp).
   
*   [CircleF.h](Engine/CircleF.h).
*   [CircleF.cpp](Engine/CircleF.cpp).

*   [Defender.h](Engine/Defender.h).
*   [Defender.cpp](Engine/Defender.cpp).

*   [Enemy.h](Engine/Enemy.h).
*   [Enemy.cpp](Engine/Enemy.cpp).

*   [Explosion.h](Engine/Explosion.h).

*   [FrameTimer.h](Engine/FrameTimer.h).
*   [FrameTimer.cpp](Engine/FrameTimer.cpp).

*   [img.h](Engine/img.h). // this class becomes obsolete using the modified Surface class(Galaxy2).
*   [img.cpp](Engine/img.cpp).// this class becomes obsolete using the modified Surface class(Galaxy2).

*   [Surface.h](Engine/Surface.h).//carries a special focus on replacing the IMG class.
*   [Surface.cpp](Engine/Surface.cpp).//and increasing the efficiency of the Framework 

*   [menu.h](Engine/menu.h).//This class is abandoned as an idea, Glaxy2 solves the problem through the Button class.
*   [menu.cpp](Engine/menu.cpp).

*   [Numbers.h](Engine/Numbers.h).
*   [Numbers.cpp](Engine/Numbers.cpp).

*   [RectF.h](Engine/RectF.h).
*   [RectF.cpp](Engine/RectF.cpp).

*   [rng.h](Engine/rng.h).

*   [Space.h](Engine/Space.h).
*   [Space.cpp](Engine/Space.cpp).

*   [Text.h](Engine/Text.h).
*   [Text.cpp](Engine/Text.cpp).

*   [Vec2.h](Engine/Vec2.h).
*   [Vec2.cpp](Engine/Vec2.cpp).

*   [Game.h](Engine/Game.h).
*   [Game.cpp](Engine/Game.cpp).

